import { createEvent, createStore } from 'effector';
import { persist } from 'effector-storage/local';

export const $favoritesIds = createStore<string[]>([]);

export const addToFavorites = createEvent<string>();
export const removeFromFavorites = createEvent<string>();

$favoritesIds
  .on(addToFavorites, (state, id) => [id, ...state])
  .on(removeFromFavorites, (state, id) => state.filter((item) => item !== id));

persist({
  store: $favoritesIds,
  key: 'favoritesIds',
  sync: 'force',
});
