'use server';

import type { ProductCard } from './types';

export const getPopular = async () => {
  try {
    const url = process.env.SERVER_URL ?? '';
    const response = await fetch(url + 'recommendation/popular');

    const data = (await response.json()) as ProductCard[];

    return data;
  } catch (error) {
    console.error(error);
  }
};
