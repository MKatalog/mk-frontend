'use server';

import { Statistics } from '@/components/organisms/Product';

const sortArrays = (item: Statistics): Statistics => {
  const combinedArray = item.x.map((isoString, index) => ({ isoString, number: item.y[index] }));

  const set = new Set(combinedArray);

  const sortedArray = Array.from(set).sort(
    (a, b) => new Date(a.isoString).getTime() - new Date(b.isoString).getTime()
  );

  return {
    title: item.title,
    type: item.type,
    x: sortedArray.map((item) => item.isoString),
    y: sortedArray.map((item) => item.number),
  };
};

export const getStatistic = async (id: string) => {
  try {
    const url = process.env.SERVER_URL ?? '';
    const response = await fetch(url + `analytic/?id=${id}`);

    const data = (await response.json()) as Statistics[];

    const newData = data.map(sortArrays);

    return newData;
  } catch (error) {
    console.error(error);
  }
};
