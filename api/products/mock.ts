import { mockCards } from '@/components/molecules/Card/mock';
import type { ProductSearch } from './types';

export const mockSearch: ProductSearch = {
  data: mockCards,
  filters: [
    {
      type: 'Category',
      title: 'Категория',
      values: [
        {
          label: 'Микросхемы',
          value: '1',
        },
        {
          label: 'Прочее',
          value: '2',
        },
      ],
    },
    {
      type: 'Price',
      title: 'Цена',
    },
    {
      type: 'DeliveryTimes',
      title: 'Срок поставки',
    },
    {
      type: 'Quantitie',
      title: 'Количество на складе',
    },
    {
      type: 'Producer',
      title: 'Производитель',
      values: [
        {
          label: 'Platan',
          value: '1',
        },
        {
          label: 'Ampero',
          value: '2',
        },
        {
          label: 'ChipDip',
          value: '3',
        },
        {
          label: 'Aliexpress',
          value: '4',
        },
      ],
    },
  ],
  breadCrumbs: [],
  total: mockCards.length,
};
