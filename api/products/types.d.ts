import { FilterSelectionType } from '@/components/molecules/FilterField/types';

export interface BeetweenStatement<T> {
  from: T;
  to: T;
}

export interface ProductCard {
  id: string;
  name: string;
  productType: string;
  imagePath?: string;
  productLinksCount: number;
  prices: BeetweenStatement<number>;
  quantites: BeetweenStatement<number>;
  deliveryTimes: BeetweenStatement<string>;
}

export type FilterType = 'Category' | 'Price' | 'DeliveryTimes' | 'Quantitie' | 'Producer';

export interface Filter {
  type: FilterType;
  title: string;
  between?: {
    from: string;
    to: string;
  };
  equal?: string;
  contains?: string[];
  values?: FilterSelectionType[];
}

export interface BreadCrumbs {
  name: string;
  categoryId: string;
  sequence: number;
}

export interface ProductSearch {
  data: ProductCard[];
  filters: Filter[];
  breadCrumbs: BreadCrumbs[];
  total: number;
}

export interface ProductSearchProps {
  productName: string;
  totalCount: number;
  page: number;
  categoryId?: string;
  filters?: Filter[];
  sort?: string;
}
