'use server';

import type { ProductCard } from './types';

export const getRecommendations = async (page: number, size: number) => {
  try {
    const url = process.env.SERVER_URL ?? '';
    const response = await fetch(url + `recommendation?page=${page}&size=${size}`);

    const data = (await response.json()) as ProductCard[];

    return data;
  } catch (error) {
    console.error(error);
  }
};
