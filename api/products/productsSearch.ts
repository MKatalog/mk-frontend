'use server';

import { mockSearch } from './mock';
import type { ProductSearch, ProductSearchProps } from './types';

export async function productsSearch(props: ProductSearchProps): Promise<ProductSearch> {
  return mockSearch;
}
