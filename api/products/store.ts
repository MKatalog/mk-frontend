import { createEffect, createEvent, createStore, sample } from 'effector';
import type { ProductSearch, ProductSearchProps } from './types';
import { productsSearch } from '.';

export const $searchData = createStore<ProductSearchProps | null>(null);
export const $search = createStore<ProductSearch | null>(null);

export const initSearch = createEvent<string>();
export const filterData = createEvent<ProductSearchProps>();

type ProductRecord = {
  data: ProductSearch;
  filters: ProductSearchProps;
};

const getProducts = async (filters: ProductSearchProps): Promise<ProductRecord> => {
  const data = await productsSearch(filters);

  return {
    data,
    filters,
  };
};

const initSearchFx = createEffect((productName: string) =>
  getProducts({ page: 0, productName, totalCount: 20 })
);

const filterDataFx = createEffect((filters: ProductSearchProps) => getProducts(filters));

$search
  .on(initSearchFx.doneData, (_, payload) => payload.data)
  .on(filterDataFx.doneData, (_, payload) => payload.data);

$searchData
  .on(initSearchFx.doneData, (_, payload) => payload.filters)
  .on(filterDataFx.doneData, (_, payload) => payload.filters);

sample({
  clock: initSearch,
  target: [initSearchFx],
});

sample({
  clock: filterData,
  target: [filterDataFx],
});
