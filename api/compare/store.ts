import { mockProducts } from '@/components/organisms/Compare/mock';
import { createEffect, createEvent, createStore } from 'effector';
import { persist } from 'effector-storage/local';

export const $compareIds = createStore<string[]>([]);

export const addToCompare = createEvent<string>();
export const removeFromCompare = createEvent<string>();
export const removeAllCompares = createEvent();

export const fetchProductsByIdsFx = createEffect(async () => {
  return mockProducts;
});

$compareIds
  .on(addToCompare, (state, id) => [id, ...state])
  .on(removeFromCompare, (state, id) => state.filter((item) => item !== id))
  .reset(removeAllCompares);

persist({
  store: $compareIds,
  key: 'compareIds',
  sync: 'force',
});

export const $compareType = createStore<string>('');

export const changeCompareType = createEvent<string>();

$compareType.on(changeCompareType, (_, type) => type);
