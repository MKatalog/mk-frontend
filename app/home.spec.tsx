import { test, expect, beforeEach, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import Home from './page';
import { TestRouter } from '@/components/templates/TestRouter';

beforeEach(() => {
  const implementsMatchMedia = () => ({
    matches: [],
    addListener: vi.fn(),
    addEventListener: vi.fn(),
  });
  window.matchMedia = vi.fn().mockImplementation(implementsMatchMedia);
  const intersectionObserverMock = () => ({
    observe: vi.fn(),
    unobserve: vi.fn(),
    disconnect: vi.fn(),
  });
  const intersectionResizeMock = () => ({
    observe: vi.fn(),
    unobserve: vi.fn(),
    disconnect: vi.fn(),
  });
  window.IntersectionObserver = vi.fn().mockImplementation(intersectionObserverMock);
  window.ResizeObserver = vi.fn().mockImplementation(intersectionResizeMock);
});

test('HomePage appears', async () => {
  render(
    <TestRouter router={{}}>
      <div data-testid="test">{await Home()}</div>
    </TestRouter>
  );
  expect(screen.getByTestId('test')).toBeDefined();
});
