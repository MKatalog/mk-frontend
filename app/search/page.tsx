'use client';

import { Sidebar } from '@/components/organisms/Sidebar';
import { FormProvider, useForm } from 'react-hook-form';
import { useEffect } from 'react';
import { useUnit } from 'effector-react';
import { $searchData, ProductSearchProps, initSearch, filterData, $search } from '@/api/products';
import { Main } from '@/components/atoms/Main';
import { Card } from '@/components/molecules/Card';

type Props = {
  searchParams: {
    text: string;
  };
};

export default function SearchPage(props: Props) {
  const [apiProps, search, initSearchFn, filterDataFn] = useUnit([
    $searchData,
    $search,
    initSearch,
    filterData,
  ]);

  const methods = useForm<ProductSearchProps>();

  useEffect(() => {
    if (!apiProps) initSearchFn(props.searchParams.text);
    if (apiProps) {
      methods.reset(apiProps);
    }
  }, [apiProps]);

  useEffect(() => {
    if (apiProps) initSearchFn(props.searchParams.text);
  }, [props.searchParams]);

  return (
    <Main>
      <section className="w-full pt-2">
        <p>электроника/микросхемы</p>
        <h3 className="text-3xl">
          {props.searchParams.text} найдено{' '}
          <span className="text-green-500">{search?.total ?? 0}</span> товаров
        </h3>
      </section>
      <FormProvider {...methods}>
        <div className="w-full flex gap-8">
          <Sidebar />
          <div className="grid min-[320px]:grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 gap-6">
            {search &&
              search.data.map((item, index) => <Card key={'card-search-' + index} data={item} />)}
          </div>
        </div>
      </FormProvider>
    </Main>
  );
}
