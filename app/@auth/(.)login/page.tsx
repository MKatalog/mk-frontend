'use client';

import { Button } from '@/components/atoms/Button';
import { Input } from '@/components/atoms/Input';
import { Modal } from '@/components/templates/Modal';
import { useRouter } from 'next/navigation';
import InputMask from 'react-input-mask';

export default function Login() {
  const { back } = useRouter();

  return (
    <Modal title="Войти или зарегистрироваться" onSubmit={() => back()}>
      <InputMask
        mask={'+7 (999) 999 99-99'}
        placeholder="+7 (___) ___ __-__"
        className="transition-colors flex h-8 w-full rounded-md border border-gray-300 bg-white px-3 py-2 text-sm ring-offset-white file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-gray-500 focus-visible:outline-none hover:border-gray-400 focus-visible:border-gray-400 disabled:cursor-not-allowed disabled:opacity-50 dark:border-gray-800 dark:hover:border-green-500 dark:focus-visible:border-green-500 dark:bg-gray-950 dark:text-white dark:placeholder:text-gray-400"
      />
      <Button>Войти</Button>
    </Modal>
  );
}
