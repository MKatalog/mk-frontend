import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import { Header } from '@/components/organisms/Header';
import { Footer } from '@/components/organisms/Footer';
import { cn } from '@/lib/utils';
import Favicon from './favicon.ico';

const inter = Inter({
  subsets: ['cyrillic', 'latin'],
  weight: ['400', '500'],
});

export const metadata: Metadata = {
  title: 'MKatalog',
  description: 'Default page',
  icons: {
    icon: Favicon.src,
  },
};

export default function RootLayout({
  auth,
  children,
}: Readonly<{
  auth: React.ReactNode;
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body
        className={cn(
          'min-[100vh] overflow-x-hidden [&:has(#modal)]:overflow-hidden',
          inter.className
        )}
      >
        <Header />
        {auth}
        {children}
        <Footer />
      </body>
    </html>
  );
}
