'use server';

import { getStatistic } from '@/api/products';
import { Main } from '@/components/atoms/Main';
import {
  ProductCharacteristics,
  ProductCharts,
  ProductImage,
  ProductOffers,
} from '@/components/organisms/Product';
import { mockProduct } from '@/components/organisms/Product/mock';
import { ProductLayout } from '@/components/templates/ProductLayout';
import type { Metadata } from 'next';

type Props = {
  params: { id: string };
  searchParams: {
    name: string;
  };
};

export async function generateMetadata({ searchParams }: Props): Promise<Metadata> {
  return {
    title: `${searchParams.name} | MKatalog`,
  };
}

export default async function ProductPage({ params }: Props) {
  const stats = await getStatistic(params.id);
  return (
    <Main>
      <h2 className="w-full text-6xl">{mockProduct.name}</h2>
      <ProductLayout
        sections={[
          <>
            <ProductImage key={'product-image'} image={mockProduct.image} name={mockProduct.name} />
            <ProductCharacteristics
              key={'product-characteristics'}
              characteristics={mockProduct.characteristics}
            />
          </>,
          <ProductCharts key={'product-statistic'} stats={stats ?? []} />,
        ]}
      />
      <ProductOffers key={'product-offers'} offers={mockProduct.productLinks} />
    </Main>
  );
}
