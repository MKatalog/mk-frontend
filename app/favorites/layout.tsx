import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Понравившиеся товары | MKatalog',
  description: 'Default page',
};

export default function Layout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return <>{children}</>;
}
