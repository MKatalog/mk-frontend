'use client';

import { $favoritesIds } from '@/api/favorites';
import { Main } from '@/components/atoms/Main';
import { Card } from '@/components/molecules/Card';
import { mockCards } from '@/components/molecules/Card/mock';
import { useUnit } from 'effector-react';
import { useEffect, useState } from 'react';

export default function Favorites() {
  const [ids, setIds] = useState<string[]>([]);

  const favorites = useUnit($favoritesIds);

  useEffect(() => {
    setIds(favorites);
  }, [favorites.length]);

  return (
    <Main>
      <section className="w-full">
        <h2 className="text-5xl/[70px]">Понравившиеся товары</h2>
        <div className="grid min-[320px]:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-6">
          {mockCards
            .filter((item) => ids.includes(item.id))
            .map((item, index) => (
              <Card key={'card-favorite-' + index} data={item} />
            ))}
        </div>
      </section>
    </Main>
  );
}
