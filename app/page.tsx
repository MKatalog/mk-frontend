'use server';

import { getPopular, getRecommendations } from '@/api/products';
import { Main } from '@/components/atoms/Main';
import { Card } from '@/components/molecules/Card';
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from '@/components/molecules/Carousel';
import { Recommendations } from '@/components/molecules/Recommendations';

export default async function Home() {
  const popular = await getPopular();
  const recommendations = await getRecommendations(0, +(process.env.RECS_PER_PAGE ?? 20));

  return (
    <Main>
      <section className="w-[100%]">
        <h2 className="text-5xl/[70px]">Популярные товары</h2>
        <Carousel opts={{ align: 'start' }} className="w-full">
          <CarouselContent>
            {popular &&
              popular.map((item, index) => (
                <CarouselItem
                  key={'card-carousel-' + index}
                  className="min-[320px]:basis-[100%] lg:basis-1/2 xl:basis-[33%] 2xl:basis-[22%]"
                >
                  <Card data={item} />
                </CarouselItem>
              ))}
          </CarouselContent>
          <CarouselPrevious />
          <CarouselNext />
        </Carousel>
      </section>
      <section className="w-[100%]">
        <h2 className="text-5xl/[70px]">Рекомендованные товары</h2>
        <Recommendations initialData={recommendations} />
      </section>
    </Main>
  );
}
