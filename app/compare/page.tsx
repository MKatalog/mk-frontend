'use client';

import { $compareIds } from '@/api/compare/store';
import { Loading } from '@/components/atoms/Loading';
import { Main } from '@/components/atoms/Main';
import { CompareLayout } from '@/components/organisms/Compare';
import { useUnit } from 'effector-react';
import { Suspense } from 'react';

export default function Compare() {
  const compareIds = useUnit($compareIds);

  return (
    <Main>
      <div className="w-full">
        <h2 className="text-5xl/[70px]">Сравнение товаров</h2>
        <Suspense fallback={<Loading />}>
          <CompareLayout ids={compareIds} />
        </Suspense>
      </div>
    </Main>
  );
}
