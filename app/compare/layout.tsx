import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Сравнение товаров | MKatalog',
  description: 'Default page',
};

export default function Layout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return <>{children}</>;
}
