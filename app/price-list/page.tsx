'use server';

import { AuthRequires } from '@/components/pages/AuthRequires';
import { InDevelop } from '@/components/pages/InDevelop';
import { cookies } from 'next/headers';

export default async function PriceList() {
  const cookie = cookies();
  const token = cookie.get('access-token');

  if (!token) return <AuthRequires />;

  return <InDevelop />;
}
