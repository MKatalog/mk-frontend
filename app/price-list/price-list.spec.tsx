import { test, expect, beforeEach, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import PriceList from './page';
import { TestRouter } from '@/components/templates/TestRouter';

beforeEach(() => {
  const implementsMatchMedia = () => ({
    matches: [],
    addListener: vi.fn(),
    addEventListener: vi.fn(),
  });
  window.matchMedia = vi.fn().mockImplementation(implementsMatchMedia);
  const intersectionObserverMock = () => ({
    observe: vi.fn(),
    unobserve: vi.fn(),
    disconnect: vi.fn(),
  });
  const intersectionResizeMock = () => ({
    observe: vi.fn(),
    unobserve: vi.fn(),
    disconnect: vi.fn(),
  });
  window.IntersectionObserver = vi.fn().mockImplementation(intersectionObserverMock);
  window.ResizeObserver = vi.fn().mockImplementation(intersectionResizeMock);
});

vi.mock('next/headers', async (importOriginal) => {
  return {
    cookies: () => {
      return {
        get: (name: string) => {
          return {
            value: 'cookie',
          };
        },
      };
    },
  };
});

test('PriceListPage appears', async () => {
  render(
    <TestRouter router={{}}>
      <div data-testid="test">{await PriceList()}</div>
    </TestRouter>
  );
  expect(screen.getByTestId('test')).toBeDefined();
});
