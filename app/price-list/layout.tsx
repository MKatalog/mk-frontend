import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Закупочные листы | MKatalog',
  description: 'Default page',
};

export default function Layout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return <>{children}</>;
}
