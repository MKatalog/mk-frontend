import type { Config } from 'tailwindcss';
import { white, gray, transparent } from 'tailwindcss/colors';

const config = {
  darkMode: ['class'],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
  ],
  prefix: '',
  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px',
      },
    },
    colors: {
      green: {
        300: '#9FFFD3',
        400: '#7CFFC3',
        500: '#50FFB1',
        600: '#43D494',
        700: '#35AA76',
      },
      gray,
      white,
      transparent,
    },
    extend: {
      keyframes: {
        'accordion-down': {
          from: { height: '0' },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: '0' },
        },
        'black-green': {
          '0%': { stroke: 'black' },
          '50%': { stroke: '#50FFB1' },
          '100%': { stroke: 'black' },
        },
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
        'black-green': 'black-green 1.5s ease, spin 1.5s ease-in-out',
      },
    },
  },
  plugins: [require('tailwindcss-animate')],
} satisfies Config;

export default config;
