export interface ModalProps {
  onSubmit?: () => void;
  title?: string;
  children?: JSX.Element | React.ReactNode;
  className?: HTMLDivElement['className'];
}
