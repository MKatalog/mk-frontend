import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Modal } from './Modal';
import { TestRouter } from '../TestRouter';

test('Modal appears', () => {
  render(
    <div data-testid="modal">
      <TestRouter router={{}}>
        <Modal />
      </TestRouter>
    </div>
  );
  expect(screen.getByTestId('modal')).toBeDefined();
});
