'use client';

import { useRouter } from 'next/navigation';
import { ModalProps } from './types';
import { cn } from '@/lib/utils';
import { MouseEventHandler, useEffect, useRef } from 'react';

export function Modal({ title, children, className, onSubmit }: ModalProps) {
  const modalRef = useRef<HTMLDivElement | null>(null);

  const { back } = useRouter();

  const onBackgroundClickHandler = (e: MouseEvent) => {
    if (modalRef.current && e.target && e.target === modalRef.current) back();
  };

  useEffect(() => {
    if (modalRef.current) {
      window.addEventListener('click', onBackgroundClickHandler, false);
    }

    return () => window.removeEventListener('click', onBackgroundClickHandler, false);
  }, [modalRef.current]);

  return (
    <div
      ref={modalRef}
      id="modal"
      className="absolute w-[100vw] z-50 h-[100vh] bg-gray-500 bg-opacity-40 flex justify-center items-center"
    >
      <dialog
        className={cn(
          'bg-white dark:bg-gray-950 p-8 flex justify-center rounded-lg flex-col min-w-[300px] max-w-[400px] gap-3',
          className
        )}
      >
        <h3 className="min-[320px]:text-2xl sm:text-3xl">{title}</h3>
        <form className="flex flex-col gap-3" action={onSubmit}>
          {children}
        </form>
      </dialog>
    </div>
  );
}
