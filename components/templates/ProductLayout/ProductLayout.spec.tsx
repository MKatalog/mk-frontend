import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { ProductLayout } from './ProductLayout';

test('ProductLayout appears', () => {
  render(
    <div data-testid="product-layout">
      <ProductLayout sections={[]} />
    </div>
  );
  expect(screen.getByTestId('product-layout')).toBeDefined();
});
