'use client';

import { cn } from '@/lib/utils';

type Props = {
  sections: React.ReactNode[];
};

export function ProductLayout({ sections }: Props) {
  return (
    <section className="w-full grid min-[320px]:grid-cols-1 sm:grid-cols-3 gap-y-10">
      {sections.map((artc, index) => (
        <article
          key={'product-article-' + index}
          className={cn('', index % 2 == 0 && 'col-span-2')}
        >
          {artc}
        </article>
      ))}
    </section>
  );
}
