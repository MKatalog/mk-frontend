import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { AuthRequires } from './AuthRequires';

test('AuthRequires appears', () => {
  render(
    <div data-testid="auth-req">
      <AuthRequires />
    </div>
  );
  expect(screen.getByTestId('auth-req')).toBeDefined();
});
