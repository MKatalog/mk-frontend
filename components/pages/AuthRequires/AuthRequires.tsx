import { EngineGearIcon } from '@/components/atoms/Icons';
import { Main } from '@/components/atoms/Main';
import Link from 'next/link';

export function AuthRequires() {
  return (
    <Main variant="center">
      <EngineGearIcon className="animate-[spin_8s_linear_infinite] " />
      <article className="text-2xl max-w-[350px] text-center">
        <h4>Чтобы воспользоваться формированием закупочного листа необходимо</h4>
        <Link href={'/login'} className="text-green-500">
          авторизоваться
        </Link>
      </article>
    </Main>
  );
}
