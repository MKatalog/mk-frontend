import { CodeDevIcon } from '@/components/atoms/Icons';
import { Main } from '@/components/atoms/Main';

export function InDevelop() {
  return (
    <Main variant="center">
      <section className="flex justify-center items-center flex-col">
        <CodeDevIcon />
        <h4 className="text-2xl">Находится в разработке</h4>
      </section>
    </Main>
  );
}
