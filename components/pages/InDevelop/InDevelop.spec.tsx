import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { InDevelop } from './InDevelop';

test('InDevelop appears', () => {
  render(
    <div data-testid="in-develop">
      <InDevelop />
    </div>
  );
  expect(screen.getByTestId('in-develop')).toBeDefined();
});
