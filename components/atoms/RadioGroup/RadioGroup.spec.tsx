import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { RadioGroup } from './RadioGroup';

test('RadiGroup appears', () => {
  render(<RadioGroup data-testid="test" />);
  expect(screen.getByTestId('test')).toBeDefined();
});
