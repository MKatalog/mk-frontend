import type { Meta, StoryObj } from '@storybook/react';
import { RadioGroup, RadioGroupItem } from './RadioGroup';

const meta: Meta<typeof RadioGroup> = {
  component: RadioGroup,
  title: 'Atoms/RadioGroup',
};

export default meta;

type Story = StoryObj<typeof RadioGroup>;

export const Primary: Story = {
  render: () => (
    <RadioGroup>
      <div className="flex items-center space-x-2">
        <RadioGroupItem value="1" id="1" />
        <label htmlFor="1">One</label>
      </div>
      <div className="flex items-center space-x-2">
        <RadioGroupItem value="2" id="2" />
        <label htmlFor="2">Two</label>
      </div>
      <div className="flex items-center space-x-2">
        <RadioGroupItem value="3" id="3" />
        <label htmlFor="3">Three</label>
      </div>
    </RadioGroup>
  ),
};
