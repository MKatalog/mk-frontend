import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Loading } from './Loading';

test('Loading appears', () => {
  render(<Loading />);
  expect(screen.getByTestId('loading')).toBeDefined();
});
