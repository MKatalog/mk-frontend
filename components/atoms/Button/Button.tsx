import * as React from 'react';
import { Slot } from '@radix-ui/react-slot';
import { cva, type VariantProps } from 'class-variance-authority';
import { cn } from '@/lib/utils';

const buttonVariants = cva(
  'inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium ring-offset-white transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-gray-950 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 dark:ring-offset-gray-950 dark:focus-visible:ring-gray-300',
  {
    variants: {
      variant: {
        primary:
          'bg-green-500 text-gray-950 hover:bg-green-500/80 dark:bg-green-500 dark:text-white dark:hover:bg-green-500/80',
        secondary:
          'bg-gray-50/50 border-2 border-gray-300 text-gray-950 hover:bg-gray-100/80 hover:border-gray-400 dark:bg-gray-900 dark:border-2 dark:border-green-500 dark:text-white dark:hover:bg-gray-900/80 dark:border-green-500/80',
        ghost:
          'hover:bg-gray-100 hover:text-gray-900 dark:hover:bg-gray-800 dark:hover:text-gray-50 dark:text-white',
        link: 'text-gray-900 underline-offset-4 hover:underline dark:text-gray-50',
        outline:
          'border border-gray-200 bg-white hover:bg-gray-100 hover:text-gray-900 dark:border-gray-800 dark:text-white dark:bg-gray-950 dark:hover:bg-gray-800 dark:hover:text-gray-50',
      },
      size: {
        sm: 'h-8 rounded-md px-4 py-2',
        lg: 'h-11 rounded-md px-4',
        icon: 'h-10 w-10',
      },
    },
    defaultVariants: {
      variant: 'primary',
      size: 'sm',
    },
  }
);

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement>,
    VariantProps<typeof buttonVariants> {
  asChild?: boolean;
}

const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
  ({ className, variant, size, asChild = false, ...props }, ref) => {
    const Comp = asChild ? Slot : 'button';
    return (
      <Comp className={cn(buttonVariants({ variant, size, className }))} ref={ref} {...props} />
    );
  }
);
Button.displayName = 'Button';

export { Button, buttonVariants };
