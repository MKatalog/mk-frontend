import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Button } from './Button';

test('Button appears', () => {
  render(<Button>Button</Button>);
  expect(screen.getByText('Button')).toBeDefined();
});
