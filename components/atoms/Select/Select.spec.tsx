import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Select } from './Select';

test('Select appears', () => {
  render(<Select />);
  expect(screen.getByDisplayValue('')).toBeDefined();
});
