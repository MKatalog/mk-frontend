import type { IconType } from './types';

export function CrossIcon(props: IconType) {
  const { pathProps, ...svgProps } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      {...svgProps}
    >
      <path
        d="M4.94977 4.94971L14.8493 14.8492"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
      <path
        d="M4.94977 14.8492L14.8493 4.94975"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
    </svg>
  );
}
