import { cn } from '@/lib/utils';
import type { IconType } from './types';

export function PriceListIcon(props: IconType) {
  const { pathProps, ...svgProps } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="21"
      height="24"
      viewBox="0 0 21 24"
      fill="none"
      className={cn('dark:[&>*]:stroke-gray-300 group', svgProps?.className)}
      {...svgProps}
    >
      <mask id="path-1-inside-1_231_228" fill="white">
        <rect width="21" height="24" rx="1" />
      </mask>
      <rect
        width="21"
        height="24"
        rx="1"
        stroke="black"
        strokeWidth="3"
        mask="url(#path-1-inside-1_231_228)"
        {...pathProps}
      />
      <path
        d="M3.3158 4.57141L7.73685 9.14284"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
      <path
        d="M7.73688 9.14284L12.1579 4.57141"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
      <path
        d="M12.1579 4.57141H17.6842"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
      <path
        d="M3.3158 12.5714H17.6842"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
      <path
        d="M3.3158 16H17.6842"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
      <path
        d="M3.3158 19.4286H17.6842"
        stroke="black"
        strokeWidth="1.5"
        strokeLinecap="round"
        {...pathProps}
      />
    </svg>
  );
}
