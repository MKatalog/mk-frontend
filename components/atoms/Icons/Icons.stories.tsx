import type { Meta, StoryObj } from '@storybook/react';
import { IconType } from './types';
import {
  CalendarIcon,
  CodeDevIcon,
  CompareIcon,
  CrossIcon,
  EngineGearIcon,
  HeartIcon,
  LogoIcon,
  PriceListIcon,
  SearchIcon,
} from '.';

const meta: Meta<IconType> = {
  component: CalendarIcon,
  title: 'Atoms/Icon',
};

export default meta;

type Story = StoryObj<IconType>;

export const Icons: Story = {
  render: () => (
    <div className="flex flex-row gap-4 items-center">
      <CrossIcon />
      <SearchIcon />
      <CalendarIcon />
      <CompareIcon />
      <HeartIcon />
      <PriceListIcon />
      <LogoIcon />
      <CodeDevIcon />
      <EngineGearIcon />
    </div>
  ),
};
