import type { IconType } from './types';

export function CodeDevIcon(props: IconType) {
  const { pathProps, ...svgProps } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="200"
      height="200"
      viewBox="0 0 200 200"
      fill="none"
      {...svgProps}
    >
      <path
        d="M58.3333 66.6667L25 100L58.3333 133.333"
        stroke="black"
        strokeWidth="10"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
      <path
        d="M141.667 66.6667L175 100L141.667 133.333"
        stroke="black"
        strokeWidth="10"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
      <path
        d="M116.667 33.3333L82.1577 162.123"
        stroke="#50FFB1"
        strokeWidth="10"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
    </svg>
  );
}
