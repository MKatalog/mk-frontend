import { cn } from '@/lib/utils';
import type { IconType } from './types';

export function LogoIcon(props: IconType) {
  const { pathProps, ...svgProps } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="88"
      height="48"
      viewBox="0 0 88 48"
      fill="none"
      {...svgProps}
    >
      <path
        d="M46.736 42V7.44H54.224V20.976H60.992L72.32 7.44H80.096V10.032L67.76 24.72L80.096 39.408V42H72.32L60.992 28.464H54.224V42H46.736Z"
        fill="#50FFB1"
        {...pathProps}
      />
      <path
        d="M8.688 42V7.44H16.368L28.08 21.408L39.744 7.44H47.472V42H39.984V18.816L28.08 33.024L16.128 18.864V42H8.688Z"
        fill="black"
        className={cn('dark:fill-gray-300', svgProps?.className)}
        {...pathProps}
      />
    </svg>
  );
}
