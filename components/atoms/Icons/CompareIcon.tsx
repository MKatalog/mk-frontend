import { cn } from '@/lib/utils';
import type { IconType } from './types';

export function CompareIcon(props: IconType) {
  const { pathProps, className, ...svgProps } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="39"
      height="33"
      viewBox="0 0 39 33"
      fill="none"
      className={cn('group', className)}
      {...svgProps}
    >
      <path
        d="M2 10.4622H21.5588"
        stroke="black"
        strokeWidth="3"
        strokeLinecap="round"
        className="transition-colors group-hover:stroke-green-500"
        {...pathProps}
      />
      <path
        d="M17.4412 21.616H37"
        stroke="#50FFB1"
        strokeWidth="3"
        strokeLinecap="round"
        className="transition-colors group-hover:stroke-gray-950"
        {...pathProps}
      />
      <path
        d="M15.3823 1.53906L21.5588 10.4621"
        stroke="black"
        strokeWidth="3"
        strokeLinecap="round"
        className="transition-colors group-hover:stroke-green-500"
        {...pathProps}
      />
      <path
        d="M21.5588 10.4622L15.3824 19.3852"
        stroke="black"
        strokeWidth="3"
        strokeLinecap="round"
        className="transition-colors group-hover:stroke-green-500"
        {...pathProps}
      />
      <path
        d="M23.6177 12.6929L17.4412 21.616"
        stroke="#50FFB1"
        strokeWidth="3"
        strokeLinecap="round"
        className="transition-colors group-hover:stroke-gray-950"
        {...pathProps}
      />
      <path
        d="M17.4412 21.616L23.6176 30.539"
        stroke="#50FFB1"
        strokeWidth="3"
        strokeLinecap="round"
        className="transition-colors group-hover:stroke-gray-950"
        {...pathProps}
      />
    </svg>
  );
}
