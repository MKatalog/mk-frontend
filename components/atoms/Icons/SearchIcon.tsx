import type { IconType } from './types';

export function SearchIcon(props: IconType) {
  const { pathProps, ...svgProps } = props;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="29"
      height="29"
      viewBox="0 0 29 29"
      fill="none"
      {...svgProps}
    >
      <circle
        cx="11.3137"
        cy="11.3137"
        r="7.25"
        transform="rotate(-45 11.3137 11.3137)"
        stroke="#07010F"
        strokeWidth="1.5"
        style={{ fill: 'none' }}
        {...pathProps}
      />
      <path
        d="M22.4507 23.5113C22.7436 23.8042 23.2185 23.8042 23.5114 23.5113C23.8042 23.2184 23.8042 22.7435 23.5114 22.4506L22.4507 23.5113ZM16.0867 17.1473L22.4507 23.5113L23.5114 22.4506L17.1474 16.0867L16.0867 17.1473Z"
        fill="#07010F"
        style={{ stroke: 'none' }}
        {...pathProps}
      />
    </svg>
  );
}
