export type IconType = Omit<SVGProps<SVGSVGElement>, 'xmlns'> & {
  pathProps?: Omit<SVGProps<SVGPathElement>, 'fillRule' | 'clipRule' | 'd'>;
};
