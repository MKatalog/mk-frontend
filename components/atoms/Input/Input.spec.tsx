import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Input } from './Input';

test('Input appears', () => {
  render(<Input data-testid="test" />);
  expect(screen.getByTestId('test')).toBeDefined();
});
