import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Checkbox } from './Checkbox';

test('Checkbox appears', () => {
  render(<Checkbox data-testid="test" />);
  expect(screen.getByTestId('test')).toBeDefined();
});
