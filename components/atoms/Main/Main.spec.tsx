import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Main } from './Main';

test('Main appears', () => {
  render(<Main data-testid="test" />);
  expect(screen.getByTestId('test')).toBeDefined();
});
