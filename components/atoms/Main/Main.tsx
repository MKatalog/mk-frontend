'use client';

import { cn } from '@/lib/utils';
import { cva } from 'class-variance-authority';
import { HTMLAttributes } from 'react';

const mainVariants = cva(
  'flex min-h-[calc(100vh-124px)] flex-col items-center gap-10 xl:px-[130px] lg:px-[100px] md:px-[60px] sm:px-[30px] min-[320px]:px-[10px] min-[320px]:pt-[90px] sm:pt-[60px]',
  {
    variants: {
      variant: {
        default: '',
        center: 'justify-center items-center',
      },
      defaultVariants: {
        variant: 'default',
      },
    },
  }
);

type Props = HTMLAttributes<HTMLElement> & {
  variant?: 'default' | 'center';
};

export function Main(props: Props) {
  const { variant, className, ...mainProps } = props;
  return <main {...mainProps} className={cn(mainVariants({ variant, className }))} />;
}
