import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Calendar } from './Calendar';

test('Calendar appears', () => {
  render(<Calendar data-testid="test" />);
  expect(screen.getByTestId('test')).toBeDefined();
});
