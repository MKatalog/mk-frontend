import type { Meta, StoryObj } from '@storybook/react';
import { Calendar } from './Calendar';

const meta: Meta<typeof Calendar> = {
  component: Calendar,
  title: 'Atoms/Calendar',
};

export default meta;

type Story = StoryObj<typeof Calendar>;

export const Primary: Story = {
  args: {},
};
