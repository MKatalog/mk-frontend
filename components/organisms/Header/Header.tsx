'use client';

import { LogoIcon } from '@/components/atoms/Icons';
import { SearchBar } from '@/components/molecules/SearchBar';
import { HeaderMenu } from './HeaderMenu';
import Link from 'next/link';
import { Suspense } from 'react';

export function Header() {
  return (
    <header
      className="w-[100%] z-10 min-h-[60px] bg-white dark:bg-gray-950 flex fixed xl:px-[130px] lg:px-[100px] md:px-[60px] sm:px-[30px] min-[320px]:px-[10px] sm:flex-row min-[320px]:flex-col border-b-2 border-gray-300 gap-3"
      data-testid="header"
    >
      <Link href={'/'} className="min-[320px]:hidden sm:flex items-center">
        <LogoIcon height="40" />
      </Link>
      <div className="min-[320px]:flex sm:hidden flex-row justify-between">
        <Link href={'/'} className="flex items-center">
          <LogoIcon height="40" />
        </Link>
        <HeaderMenu className="flex-row" />
      </div>
      <Suspense>
        <SearchBar className="w-[100%] min-[320px]:pb-2 sm:pb-0" />
      </Suspense>
      <HeaderMenu className="min-[320px]:hidden sm:flex" />
    </header>
  );
}
