import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Header } from './Header';
import { TestRouter } from '@/components/templates/TestRouter';

test('Footer appears', () => {
  render(
    <TestRouter router={{}}>
      <Header />
    </TestRouter>
  );
  expect(screen.getByTestId('header')).toBeDefined();
});
