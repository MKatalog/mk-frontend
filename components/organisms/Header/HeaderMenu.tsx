'use client';

import { $compareIds } from '@/api/compare';
import { $favoritesIds } from '@/api/favorites';
import { Button } from '@/components/atoms/Button';
import { CompareIcon, HeartIcon, PriceListIcon } from '@/components/atoms/Icons';
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '@/components/atoms/Tooltip';
import { cn } from '@/lib/utils';
import { useUnit } from 'effector-react';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';

type CountType = {
  favorites: number;
  compares: number;
};

const defaultCounts: CountType = {
  compares: 0,
  favorites: 0,
};

export function HeaderMenu({ className }: { className?: string }) {
  const { push } = useRouter();

  const [counts, setCounts] = useState<CountType>(defaultCounts);
  const [favorites, compares] = useUnit([$favoritesIds, $compareIds]);
  const onLoginClickHandler = () => push('/login');

  useEffect(() => {
    setCounts({
      favorites: favorites.length,
      compares: compares.length,
    });
  }, [favorites.length, compares.length]);

  return (
    <menu className={cn('flex flex-row gap-3 items-center', className)}>
      <TooltipProvider>
        <Tooltip>
          <TooltipTrigger asChild>
            <Link href={'/compare'} className="relative">
              <div className="absolute top-0 right-0 bg-gray-950 dark:bg-green-500 text-white rounded-full text-[10px] w-[15px] text-center">
                {counts.compares}
              </div>
              <CompareIcon className="w-[33px] h-[30px] cursor-pointer" />
            </Link>
          </TooltipTrigger>
          <TooltipContent>
            <p>Сравние</p>
          </TooltipContent>
        </Tooltip>
      </TooltipProvider>
      <TooltipProvider>
        <Tooltip>
          <TooltipTrigger asChild>
            <Link href={'/price-list'}>
              <PriceListIcon
                pathProps={{ className: 'transition-colors group-hover:stroke-green-500 ' }}
              />
            </Link>
          </TooltipTrigger>
          <TooltipContent>
            <p>Закупочный лист</p>
          </TooltipContent>
        </Tooltip>
      </TooltipProvider>
      <TooltipProvider>
        <Tooltip>
          <TooltipTrigger asChild>
            <Link href={'/favorites'} className="relative">
              <div className="absolute top-0 right-0 bg-gray-950 dark:bg-green-500 text-white rounded-full text-[10px] w-[15px] text-center">
                {counts.favorites}
              </div>
              <HeartIcon />
            </Link>
          </TooltipTrigger>
          <TooltipContent>
            <p>Понравившиеся товары</p>
          </TooltipContent>
        </Tooltip>
      </TooltipProvider>
      <Button onClick={onLoginClickHandler}>Войти</Button>
    </menu>
  );
}
