'use client';

import Link from 'next/link';

export function Footer() {
  const copyTextToClipboard = async (e: React.MouseEvent<HTMLSpanElement>) => {
    if ('clipboard' in navigator) {
      return await navigator.clipboard.writeText(e?.currentTarget?.textContent ?? '');
    } else {
      return document.execCommand('copy', true, e?.currentTarget?.textContent ?? '');
    }
  };
  return (
    <footer
      className="flex min-[320px]:justify-center min-[320px]:flex-col min-[320px]:[&>div]:items-center sm:[&>div]:items-start sm:flex-row min-[320px]:gap-y-4 sm:justify-between sm:gap-y-0 py-4 flex-wrap xl:px-[130px] lg:px-[100px] md:px-[60px] sm:px-[30px] min-[320px]:px-[10px] bg-gray-200 dark:bg-gray-600 dark:text-white text-sm"
      data-testid="footer"
    >
      <div className="flex flex-col">
        <h4 className="text-2xl">Об сервисе</h4>
        <Link href={'/'}>Какие сайты подключены?</Link>
        <Link href={'/'}>Как устроены рекомендации?</Link>
        <Link href={'/'}>Как работать с сервисом?</Link>
      </div>
      <div className="flex flex-col">
        <h4 className="text-2xl">Контакты</h4>
        <p>
          Тел:{' '}
          <span className="cursor-pointer" onClick={copyTextToClipboard}>
            +7 (999) 111-55-55
          </span>
        </p>
        <p>
          Почта:{' '}
          <span className="cursor-pointer" onClick={copyTextToClipboard}>
            micro-k.cont@diplom.ru
          </span>
        </p>
      </div>
      <div className="flex flex-col">
        <h4 className="text-2xl">Поддержка</h4>
        <p>
          Тел:{' '}
          <span className="cursor-pointer" onClick={copyTextToClipboard}>
            +7 (999) 111-35-35
          </span>
        </p>
        <p>
          Почта:{' '}
          <span className="cursor-pointer" onClick={copyTextToClipboard}>
            micro-k.sup@diplom.ru
          </span>
        </p>
      </div>
    </footer>
  );
}
