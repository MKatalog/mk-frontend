import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Footer } from './Footer';

test('Footer appears', () => {
  render(<Footer />);
  expect(screen.getByTestId('footer')).toBeDefined();
});
