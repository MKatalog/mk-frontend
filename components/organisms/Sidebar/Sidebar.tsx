'use client';

import { $search, type Filter, type FilterType, type ProductSearchProps } from '@/api/products';
import { FilterField } from '@/components/molecules/FilterField';
import { useUnit } from 'effector-react';
import { useFieldArray, useFormContext } from 'react-hook-form';

export function Sidebar() {
  const { getValues, control } = useFormContext<ProductSearchProps>();
  const { fields } = useFieldArray<ProductSearchProps>({
    control,
    name: 'filters',
  });

  const search = useUnit($search);

  const getDefaultFilterByFilterType = (filter: Filter, index: number) => {
    switch (filter.type) {
      case 'Category':
        return (
          <FilterField
            key={`${filter.type}-${index}`}
            type="radio"
            title={filter.title}
            values={filter.values ?? []}
          />
        );
      case 'Price':
        return <FilterField key={`${filter.type}-${index}`} type="number" title={filter.title} />;
      case 'DeliveryTimes':
        return <FilterField key={`${filter.type}-${index}`} type="date" title={filter.title} />;
      case 'Quantitie':
        return <FilterField key={`${filter.type}-${index}`} type="number" title={filter.title} />;
      case 'Producer':
        return (
          <FilterField
            key={`${filter.type}-${index}`}
            type="checkbox"
            title={filter.title}
            values={filter.values ?? []}
          />
        );
    }
  };

  return (
    <aside className="w-fit w-max-[280px] h-fit">
      <form className="flex flex-col gap-2 w-full h-full">
        {search?.filters &&
          search.filters.map((filter, index) => getDefaultFilterByFilterType(filter, index))}
      </form>
    </aside>
  );
}
