import { test, expect } from 'vitest';
import { render, renderHook, screen } from '@testing-library/react';
import { Sidebar } from './Sidebar';
import { FormProvider, useForm } from 'react-hook-form';

test('Sidebar appears', () => {
  const { result } = renderHook(() => useForm());
  render(
    <div data-testid="sidebar">
      <FormProvider {...result.current}>
        <Sidebar />
      </FormProvider>
    </div>
  );
  expect(screen.getByTestId('sidebar')).toBeDefined();
});
