import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { ProductCompare } from './ProductCompare';

test('ProductCompare appears', () => {
  render(
    <div data-testid="product-compare">
      <ProductCompare />
    </div>
  );
  expect(screen.getByTestId('product-compare')).toBeDefined();
});
