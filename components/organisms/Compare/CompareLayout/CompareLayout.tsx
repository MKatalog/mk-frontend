import { fetchProductsByIdsFx } from '@/api/compare';
import { CompareSelect, CompareTable } from '..';

type Props = {
  ids: string[];
};

export async function CompareLayout({ ids }: Props) {
  const products = await fetchProductsByIdsFx();
  return (
    <>
      <CompareSelect products={products} />
      <CompareTable products={products} />
    </>
  );
}
