import type { Product } from '../Product';
import { StatisticType } from '../Product/utils';

export const mockProducts: Product[] = [
  {
    id: '1',
    name: 'STM-32',
    productType: 'Микроконтроллер',
    image:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    statistic: [],
    characteristics: [
      {
        label: 'Вольтаж',
        value: '0.3 B',
      },
      {
        label: 'Макс. нагрузка',
        value: '0.3 B',
      },
      {
        label: 'Сопротивление',
        value: '220 Ом',
      },
      {
        label: 'Модель',
        value: 'STM',
      },
    ],
    productLinks: [],
  },
  {
    id: '2',
    name: 'STM-32',
    productType: 'Микроконтроллер',
    image:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    statistic: [],
    characteristics: [
      {
        label: 'Вольтаж',
        value: '0.3 B',
      },
      {
        label: 'Макс. нагрузка',
        value: '0.3 B',
      },
      {
        label: 'Сопротивление',
        value: '220 Ом',
      },
      {
        label: 'Модель',
        value: 'STM',
      },
    ],
    productLinks: [],
  },
  {
    id: '3',
    name: 'STM-32',
    productType: 'Прочее',
    image:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    statistic: [],
    characteristics: [
      {
        label: 'Вольтаж',
        value: '0.3 B',
      },
      {
        label: 'Макс. нагрузка',
        value: '0.3 B',
      },
      {
        label: 'Сопротивление',
        value: '220 Ом',
      },
      {
        label: 'Модель',
        value: 'STM',
      },
    ],
    productLinks: [],
  },
];
