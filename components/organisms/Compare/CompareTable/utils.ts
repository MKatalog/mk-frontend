import { Characteristics, Product } from '../../Product';

type CompareTableRow = {
  values: string[];
  isHeading: boolean;
};

function mapperCharacteristicToValue(values: Characteristics[], label: string) {
  const mappedCharacteristics = values
    .filter((item) => item.label === label)
    .map((item) => item.value);

  if (mappedCharacteristics.length === 0) return '-';

  return mappedCharacteristics[0];
}

export function mapperProductsToTableCells(values: Product[]): CompareTableRow[] {
  const uniqueLabels = new Set<string>();
  const tableCells: CompareTableRow[] = [];

  values.forEach((value) => {
    value.characteristics.map((item) => item.label).forEach(uniqueLabels.add, uniqueLabels);
  });

  uniqueLabels.forEach((cell) => {
    tableCells.push({
      isHeading: true,
      values: [cell],
    });

    tableCells.push({
      isHeading: false,
      values: values.map((product) => mapperCharacteristicToValue(product.characteristics, cell)),
    });
  });

  return tableCells;
}
