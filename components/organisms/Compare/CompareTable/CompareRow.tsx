'use client';

type Props = {
  values: string[];
  isHeading: boolean;
};

export default function CompareRow({ isHeading, values }: Props) {
  if (isHeading)
    return (
      <tr className="h-[35px]" data-testid="compare-row">
        {values.map((value, index) => (
          <td key={'head-cell-' + index} className="align-bottom">
            {value}
          </td>
        ))}
      </tr>
    );
  return (
    <tr data-testid="compare-row">
      {values.map((value, index) => (
        <td key={'value-cell-' + index} className="text-gray-500">
          {value}
        </td>
      ))}
    </tr>
  );
}
