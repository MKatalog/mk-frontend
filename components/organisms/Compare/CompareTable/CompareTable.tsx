'use client';

import { ScrollArea, ScrollBar } from '@/components/atoms/ScrollArea';
import { Product } from '../../Product';
import { useUnit } from 'effector-react';
import { $compareType } from '@/api/compare/store';
import { useMemo } from 'react';
import Image from 'next/image';
import { mapperProductsToTableCells } from './utils';
import CompareRow from './CompareRow';
import { useRouter } from 'next/navigation';
import { Button } from '@/components/atoms/Button';

type Props = {
  products: Product[];
};

export function CompareTable({ products }: Props) {
  const compareType = useUnit($compareType);

  const router = useRouter();

  const filteredProducts = useMemo(
    () => products.filter((product) => product.productType === compareType),
    [compareType]
  );

  const characteristicCells = useMemo(
    () => mapperProductsToTableCells(filteredProducts),
    [filteredProducts]
  );

  const onRedirectHandler = (product: Product) => {
    router.push(`/product/${product.id}?name=${product.name}`);
  };

  if (filteredProducts.length === 0) return null;

  return (
    <ScrollArea className="w-[100%] whitespace-nowrap mt-5">
      <table data-testid="compare-table">
        <thead>
          <tr>
            {filteredProducts.map((item, index) => (
              <th key={'table-head-img-' + index} className="p-4 text-2xl">
                <Image src={item.image} alt={item.name} width={150} height={150} />
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          <tr>
            {filteredProducts.map((item, index) => (
              <td key={'compare-product-name-' + index} className="max-w-[200px] text-wrap">
                {item.name}
              </td>
            ))}
          </tr>
          {characteristicCells.map(({ values, isHeading }, index) => (
            <CompareRow key={'compare-row-' + index} values={values} isHeading={isHeading} />
          ))}
          <tr>
            {filteredProducts.map((item, index) => (
              <td key={'compare-product-action-' + index} className="max-w-[200px] text-wrap">
                <Button onClick={() => onRedirectHandler(item)}>Перейти</Button>
              </td>
            ))}
          </tr>
        </tbody>
      </table>
      <ScrollBar orientation="horizontal" />
    </ScrollArea>
  );
}
