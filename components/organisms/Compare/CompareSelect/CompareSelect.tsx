'use client';

import { $compareType, changeCompareType } from '@/api/compare/store';
import {
  Select,
  SelectContent,
  SelectTrigger,
  SelectValue,
  SelectItem,
} from '@/components/atoms/Select';
import type { Product } from '@/components/organisms/Product';
import { useUnit } from 'effector-react';
import { useEffect, useMemo } from 'react';

type Props = {
  products: Product[];
};

export function CompareSelect({ products }: Props) {
  const [compareType, changeType] = useUnit([$compareType, changeCompareType]);

  const productTypes = useMemo(
    () => Array.from(new Set(products.map((item) => item.productType))),
    [products.length]
  );

  useEffect(() => {
    if (productTypes.length > 0) changeType(productTypes[0]);
  }, []);

  return (
    <Select onValueChange={(value) => changeType(value)} value={compareType}>
      <SelectTrigger className="w-[280px]">
        <SelectValue placeholder={'Выберите товары для сравнения'} data-testid="compare-select" />
      </SelectTrigger>
      <SelectContent>
        {productTypes &&
          productTypes.map((item, index) => (
            <SelectItem key={'compare-select-item-' + index} value={item}>
              {item}
            </SelectItem>
          ))}
      </SelectContent>
    </Select>
  );
}
