import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { CompareLayout } from './CompareLayout';
import { TestRouter } from '@/components/templates/TestRouter';

test('CompareLayout appears', async () => {
  render(
    <TestRouter router={{}}>
      <div data-testid="test-compare">{await CompareLayout({ ids: [] })}</div>
    </TestRouter>
  );
  expect(screen.getByTestId('test-compare')).toBeDefined();
});

test('CompareSelect appears', () => {
  expect(screen.getByTestId('compare-select')).toBeDefined();
});

test('CompareTable appears', () => {
  expect(screen.getByTestId('compare-select')).toBeDefined();
});

test('CompareRows appears', () => {
  expect(screen.getAllByTestId('compare-row')).toBeDefined();
});
