'use client';

import { DateTime } from 'luxon';
import type { ProductLinks } from '../../types';
import { Button } from '@/components/atoms/Button';
import { cn } from '@/lib/utils';

export function ProductOffersTable({ offers }: { offers: ProductLinks[] }) {
  return (
    <table className="w-full text-sm text-left rtl:text-right text-gray-950 dark:text-white">
      <thead className="text-md text-gray-700">
        <tr>
          <th>Магазин</th>
          <th>Наименование</th>
          <th>Цена</th>
          <th>Количество</th>
          <th>Срок поставки</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {offers.map((offer, index) => (
          <tr
            key={'offer-table-item-' + index}
            className={cn(
              '',
              index !== offers.length - 1 && 'border-b border-gray-300 dark:border-gray-700'
            )}
          >
            <td>{offer.siteName}</td>
            <td>
              <a
                href={offer.link}
                target="_blank"
                className="transition-colors hover:text-green-500"
              >
                {offer.name}
              </a>
            </td>
            <td>{offer.price}</td>
            <td>{offer.quantity}</td>
            <td>{DateTime.fromISO(offer.deliveryTime).setLocale('ru').toFormat('dd.mm.yyyy')}</td>
            <td>
              <Button />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
