'use client';

import { useEffect, useState } from 'react';
import type { ProductLinks } from '../../types';
import { Button } from '@/components/atoms/Button';
import { DateTime } from 'luxon';

export function ProductOffersCard({ offers }: { offers: ProductLinks[] }) {
  const [offersToShow, setOffersToShow] = useState<ProductLinks[]>([]);
  const [next, setNext] = useState<number>(2);

  const loopWithSlice = (start: number, end: number) => {
    const slicedOffers = offers.slice(start, end);

    setOffersToShow([...offersToShow, ...slicedOffers]);
  };

  const onShowMoreDataHandler = () => {
    loopWithSlice(next, next + 2);
    setNext(next + 2);
  };

  useEffect(() => {
    if (offersToShow.length < 2) loopWithSlice(0, 2);
  }, [offers]);

  return (
    <div className="w-full flex flex-col justify-center">
      <div className="transition-[height] h-fit flex flex-wrap gap-2">
        {offersToShow.map((offer, index) => (
          <article
            key={'offer-card-' + index}
            className="transition-colors min-w-[280px] p-2 border-2 border-transparent rounded-md hover:border-gray-300 w-fit"
          >
            <h3 className="text-3xl border-b border-gray-300">{offer.name}</h3>
            <div className="grid grid-cols-2">
              <p>Магазин:</p>
              <p>{offer.siteName}</p>
              <p>Наименование:</p>
              <a
                href={offer.link}
                target="_blank"
                className="transition-colors hover:text-green-500"
              >
                {offer.name}
              </a>
              <p>Цена:</p>
              <p>{offer.price}</p>
              <p>Количество:</p>
              <p>{offer.quantity}</p>
              <p>Срок поставки:</p>
              <p>{DateTime.fromISO(offer.deliveryTime).setLocale('ru').toFormat('dd.mm.yyyy')}</p>
            </div>
            <Button className="w-full">Добавить</Button>
          </article>
        ))}
      </div>
      <Button onClick={onShowMoreDataHandler} disabled={offersToShow.length === offers.length}>
        Показать ещё
      </Button>
    </div>
  );
}
