import { ProductLinks } from '../types';

export const sortOptimalOffers = (offers: ProductLinks[]): ProductLinks[] => {
  return offers.sort((a, b) => {
    return b.price - a.price || a.quantity - b.quantity;
  });
};
