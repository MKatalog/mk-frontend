'use client';

import { ProductLinks } from '../types';
import { ProductOffersCard, ProductOffersTable } from './PructOffersViews';
import { useMediaQuery } from 'usehooks-ts';

export function ProductOffers({ offers }: { offers: ProductLinks[] }) {
  const media = useMediaQuery('(min-width: 640px)');

  return (
    <section className="w-full justify-center flex">
      {media ? <ProductOffersTable offers={offers} /> : <ProductOffersCard offers={offers} />}
    </section>
  );
}
