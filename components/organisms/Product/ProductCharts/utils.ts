import { DateTime } from 'luxon';

export const timesToProductCharts = (values: string[]): string[] => {
  const newValues: string[] = [];

  values.forEach((value, index) => {
    if (index % 2 === 0) {
      newValues.push(DateTime.fromISO(value).setLocale('ru').toFormat('MM.yyyy'));
    } else {
      newValues.push('');
    }
  });

  return newValues;
};
