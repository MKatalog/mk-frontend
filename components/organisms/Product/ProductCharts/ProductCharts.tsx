import { LineChart } from '@/components/molecules/LineChart';
import { Statistics } from '../types';
import { timesToProductCharts } from './utils';

export function ProductCharts({ stats }: { stats: Statistics[] }) {
  return (
    <>
      {stats.map((item, index) => (
        <LineChart
          key={'statistic-chart-' + index}
          id={'line-chart-' + index}
          title={item.title}
          labels={timesToProductCharts(item.x)}
          dataset={{ data: item.y }}
        />
      ))}
    </>
  );
}
