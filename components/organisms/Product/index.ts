export { ProductCharacteristics } from './ProductCharacteristics/ProductCharacteristics';
export { ProductImage } from './ProductImage/ProductImage';
export { ProductCharts } from './ProductCharts/ProductCharts';
export { ProductOffers } from './ProductOffers/ProductOffers';

export type { Characteristics, Product, ProductLinks, Statistics } from './types';
