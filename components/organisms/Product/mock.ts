import { DateTime } from 'luxon';
import type { Product } from './types';
import { StatisticType } from './utils';

export const mockProduct: Product = {
  id: '1',
  name: 'STM-32',
  productType: 'Микроконтроллер',
  image:
    'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
  statistic: [
    {
      title: 'Изменение цены',
      type: StatisticType.COST,
      x: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      y: [86, 114, 106, 106, 107, 111, 133],
    },
  ],
  characteristics: [
    {
      label: 'Вольтаж:',
      value: '0.3 B',
    },
    {
      label: 'Макс. нагрузка:',
      value: '0.3 B',
    },
    {
      label: 'Сопротивление:',
      value: '220 Ом',
    },
    {
      label: 'Модель:',
      value: 'STM',
    },
  ],
  productLinks: [
    {
      id: '1',
      productId: '1',
      name: 'STM-32',
      link: 'https://www.youtube.com/watch?v=JTu9AgwHnU4&t=1938s',
      price: 120,
      quantity: 20,
      siteName: 'Platan',
      deliveryTime: DateTime.now().plus({ day: 2 }).toISO(),
    },
    {
      id: '2',
      productId: '2',
      name: 'STM-32',
      link: 'https://www.youtube.com/watch?v=JTu9AgwHnU4&t=1938s',
      price: 100,
      quantity: 90,
      siteName: 'Aliexpress',
      deliveryTime: DateTime.now().plus({ day: 20 }).toISO(),
    },
    {
      id: '3',
      productId: '3',
      name: 'STM-32',
      link: 'https://www.youtube.com/watch?v=JTu9AgwHnU4&t=1938s',
      price: 10,
      quantity: 2000,
      siteName: 'Krasnov',
      deliveryTime: DateTime.now().plus({ day: 9 }).toISO(),
    },
    {
      id: '4',
      productId: '4',
      name: 'STM-32',
      link: 'https://www.youtube.com/watch?v=JTu9AgwHnU4&t=1938s',
      price: 130,
      quantity: 200,
      siteName: 'Ampero',
      deliveryTime: DateTime.now().plus({ day: 3 }).toISO(),
    },
    {
      id: '5',
      productId: '5',
      name: 'STM-32',
      link: 'https://www.youtube.com/watch?v=JTu9AgwHnU4&t=1938s',
      price: 220,
      quantity: 10,
      siteName: 'Amper-kot',
      deliveryTime: DateTime.now().plus({ day: 5 }).toISO(),
    },
  ],
};
