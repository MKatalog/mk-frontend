'use client';

import Image from 'next/image';

export function ProductImage({ image, name }: { image: string; name: string }) {
  return <Image src={image} alt={name} width={600} height={600} />;
}
