import type { Characteristics } from '../types';

export function ProductCharacteristics({
  characteristics,
}: {
  characteristics: Characteristics[];
}) {
  return (
    <div key={'product-characteristics'} className="pt-8">
      <h3 className="text-3xl">Характеристики</h3>
      <div className="grid grid-cols-2 [&>*:nth-child(odd)]:text-gray-500">
        {characteristics.map((charact, index) => (
          <>
            <p key={'c-' + index}>{charact.label}</p>
            <p key={'v-' + index}>{charact.value}</p>
          </>
        ))}
      </div>
    </div>
  );
}
