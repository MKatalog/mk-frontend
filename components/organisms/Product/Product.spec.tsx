import { test, expect, beforeEach, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import { ProductCharacteristics } from './ProductCharacteristics/ProductCharacteristics';
import { ProductCharts } from './ProductCharts/ProductCharts';
import { ProductImage } from './ProductImage/ProductImage';
import { mockProduct } from './mock';
import { ProductOffers } from './ProductOffers/ProductOffers';

beforeEach(() => {
  const implementsMatchMedia = () => ({
    matches: [],
    addListener: vi.fn(),
    addEventListener: vi.fn(),
  });
  window.matchMedia = vi.fn().mockImplementation(implementsMatchMedia);
  HTMLCanvasElement.prototype.getContext = vi.fn() as typeof HTMLCanvasElement.prototype.getContext;
});

test('ProductCharacteristics appears', () => {
  render(
    <div data-testid="product-characteristics">
      <ProductCharacteristics characteristics={mockProduct.characteristics} />
    </div>
  );
  expect(screen.getByTestId('product-characteristics')).toBeDefined();
});

test('ProductCharts appears', () => {
  render(
    <div data-testid="product-stats">
      <ProductCharts stats={mockProduct.statistic} />
    </div>
  );
  expect(screen.getByTestId('product-stats')).toBeDefined();
});

test('ProductImage appears', () => {
  render(
    <div data-testid="product-img">
      <ProductImage image={mockProduct.image} name={mockProduct.name} />
    </div>
  );
  expect(screen.getByTestId('product-img')).toBeDefined();
});

test('ProductOffers appears', () => {
  render(
    <div data-testid="product-offers">
      <ProductOffers offers={mockProduct.productLinks} />
    </div>
  );
  expect(screen.getByTestId('product-offers')).toBeDefined();
});
