export enum StatisticType {
  COST = 'COST',
  QUANTITY = 'QUANTITY',
  DELIVERY_TIME = 'DELIVERY_TIME',
}
