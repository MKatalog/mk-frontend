import { StatisticType } from './utils';

export interface Characteristics {
  label: string;
  value: string;
}

export interface Statistics {
  type: StatisticType;
  title: string;
  y: number[];
  x: string[];
}

export interface ProductLinks {
  id: string;
  link: string;
  siteName: string;
  name: string;
  productId: string;
  price: number;
  quantity: number;
  deliveryTime: string;
}

export interface Product {
  id: string;
  name: string;
  productType: string;
  image: string;
  characteristics: Characteristics[];
  statistic: Statistics[];
  productLinks: ProductLinks[];
}
