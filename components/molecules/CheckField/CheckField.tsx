import { Checkbox } from '@/components/atoms/Checkbox';

interface CheckFieldProps {
  label?: string;
  value?: string;
  checked?: boolean;
  onChecked?: (value: boolean) => void;
}

export function CheckField({ label, checked, onChecked, value }: CheckFieldProps) {
  const onClickHandler = () => {
    onChecked && onChecked(!checked);
  };
  return (
    <div
      className="flex items-center space-x-2 cursor-pointer"
      onClick={onClickHandler}
      data-testid="check-field"
    >
      <Checkbox id={'check-field-' + value ?? ''} checked={checked} />
      <label htmlFor={'check-field-' + value ?? ''} className="cursor-pointer text-sm">
        {label ?? ''}
      </label>
    </div>
  );
}
