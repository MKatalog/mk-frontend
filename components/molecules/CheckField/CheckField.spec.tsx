import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { CheckField } from './CheckField';

test('CheckField appears', () => {
  render(<CheckField />);
  expect(screen.getByTestId('check-field')).toBeDefined();
});
