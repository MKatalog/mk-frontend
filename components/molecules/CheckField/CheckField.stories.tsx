import type { Meta, StoryObj } from '@storybook/react';
import { useState } from 'react';
import { CheckField } from './CheckField';

const meta: Meta<typeof CheckField> = {
  component: CheckField,
  title: 'Molecules/CheckField',
};

export default meta;

type Story = StoryObj<typeof CheckField>;

const CheckFieldFn = () => {
  const [value, setValue] = useState<boolean>(false);

  return <CheckField checked={value} onChecked={setValue} label="Товар" value="1235" />;
};

export const Primary: Story = {
  render: () => <CheckFieldFn />,
};
