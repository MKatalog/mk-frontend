export type FilterFieldType = 'radio' | 'checkbox' | 'text' | 'date' | 'number';

export interface FilterDateType {
  from?: Date;
  to?: Date;
}

export interface FilterSelectionType {
  value: string;
  label: string;
}

interface FilterFieldProps {
  type: FilterFieldType;
  title?: string;
  values?: FilterSelectionType[];
}
