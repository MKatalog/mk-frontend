import { Input } from '@/components/atoms/Input';
import { FilterDateType, FilterFieldProps } from './types';
import { DatePicker } from '../DatePicker';
import { useState } from 'react';
import { RadioGroup, RadioGroupItem } from '@/components/atoms/RadioGroup';
import { CheckField } from '../CheckField';

function FilterSectionByType({ type, values }: FilterFieldProps) {
  const [date, setDate] = useState<FilterDateType>();

  const onDateChangeHandler = (key: keyof FilterDateType, value?: Date) => {
    setDate({ ...date, [key]: value });
  };
  switch (type) {
    case 'number':
      return (
        <>
          <h6 className="text-base">от</h6>
          <Input type="number" />
          <h6 className="text-base">до</h6>
          <Input type="number" />
        </>
      );
    case 'radio':
      return (
        <RadioGroup>
          {values &&
            values.map((item, index) => (
              <div
                key={'filter-radio-group-item-' + index}
                className="flex items-center space-x-2 cursor-pointer"
              >
                <RadioGroupItem value={item.value} id={'radio-group-item-' + index} />
                <label htmlFor={'radio-group-item-' + index} className="text-base cursor-pointer">
                  {item.label}
                </label>
              </div>
            ))}
        </RadioGroup>
      );
    case 'checkbox':
      return (
        <div className="flex flex-col gap-y-2">
          {values &&
            values.map((item, index) => (
              <CheckField
                key={'filter-check-item-' + index}
                value={item.value}
                label={item.label}
              />
            ))}
        </div>
      );
    case 'text':
      return (
        <>
          <h6 className="text-base">от</h6>
          <Input />
          <h6 className="text-base">до</h6>
          <Input />
        </>
      );
    case 'date':
      return (
        <>
          <h6 className="text-base">от</h6>
          <DatePicker value={date?.from} onSelect={(e) => onDateChangeHandler('from', e)} />
          <h6 className="text-base">до</h6>
          <DatePicker value={date?.to} onSelect={(e) => onDateChangeHandler('to', e)} />
        </>
      );
  }
}

export default FilterSectionByType;
