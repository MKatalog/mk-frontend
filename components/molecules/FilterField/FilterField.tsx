import FilterSectionByType from './FilterSectionByType';
import { FilterFieldProps } from './types';

export function FilterField(props: FilterFieldProps) {
  return (
    <section className="flex flex-col gap-y-2" data-testid="filter-field">
      <h5 className="text-lg">{props?.title ?? ''}</h5>
      <div className="flex gap-x-2 items-center">
        <FilterSectionByType {...props} />
      </div>
    </section>
  );
}
