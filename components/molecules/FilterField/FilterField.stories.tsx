import type { Meta, StoryObj } from '@storybook/react';
import { useState } from 'react';
import { FilterField } from './FilterField';

const meta: Meta<typeof FilterField> = {
  component: FilterField,
  title: 'Molecules/FilterField',
};

export default meta;

type Story = StoryObj<typeof FilterField>;

export const Text: Story = {
  render: () => <FilterField type="text" title="Текст" />,
};

export const Number: Story = {
  render: () => <FilterField type="number" title="Цена" />,
};

export const Date: Story = {
  render: () => <FilterField type="date" title="Срок поставки" />,
};

export const Checkbox: Story = {
  render: () => (
    <FilterField
      type="checkbox"
      title="Производитель"
      values={[
        { label: 'Китай', value: '1' },
        { label: 'Россия', value: '2' },
        { label: 'Япония', value: '3' },
      ]}
    />
  ),
};

export const Radio: Story = {
  render: () => (
    <FilterField
      type="radio"
      title="Категория"
      values={[
        { label: 'Микроконтроллеры', value: '1' },
        { label: 'Прочее', value: '2' },
      ]}
    />
  ),
};
