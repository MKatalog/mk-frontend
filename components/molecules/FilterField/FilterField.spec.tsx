import { test, expect } from 'vitest';
import { render, screen, cleanup } from '@testing-library/react';
import { FilterField } from './FilterField';

test('CheckField appears / checkbox', () => {
  render(<FilterField type="checkbox" />);
  expect(screen.getByTestId('filter-field')).toBeDefined();
  cleanup();
});

test('CheckField appears / date', () => {
  render(<FilterField type="date" />);
  expect(screen.getByTestId('filter-field')).toBeDefined();
  cleanup();
});

test('CheckField appears / number', () => {
  render(<FilterField type="number" />);
  expect(screen.getByTestId('filter-field')).toBeDefined();
  cleanup();
});

test('CheckField appears / radio', () => {
  render(<FilterField type="radio" />);
  expect(screen.getByTestId('filter-field')).toBeDefined();
  cleanup();
});

test('CheckField appears / text', () => {
  render(<FilterField type="text" />);
  expect(screen.getByTestId('filter-field')).toBeDefined();
  cleanup();
});
