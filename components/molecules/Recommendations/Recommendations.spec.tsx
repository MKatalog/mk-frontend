import { test, expect, beforeEach, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Recommendations } from './Recommendations';

beforeEach(() => {
  const intersectionObserverMock = () => ({
    observe: () => null,
  });
  window.IntersectionObserver = vi.fn().mockImplementation(intersectionObserverMock);
});

test('Recommendations appears', () => {
  render(<Recommendations />);
  expect(screen.getByTestId('recs')).toBeDefined();
});
