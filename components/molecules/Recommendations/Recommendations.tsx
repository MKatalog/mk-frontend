'use client';

import { useInView } from 'react-intersection-observer';
import { ProductCard, getRecommendations } from '@/api/products';
import { useEffect, useState } from 'react';
import { Card } from '../Card';
import { Loading } from '@/components/atoms/Loading';

type Props = {
  initialData?: ProductCard[];
};

export function Recommendations({ initialData }: Props) {
  const [page, setPage] = useState(0);
  const [recommendations, setRecommendations] = useState<ProductCard[]>(initialData ?? []);
  const { ref, inView } = useInView();

  const loadMoreRecs = async () => {
    const newRecs = await getRecommendations(page, +(process.env.RECS_PER_PAGE ?? 20));
    if (newRecs) {
      setRecommendations([...recommendations, ...newRecs]);
      setPage(page + 1);
    }
  };

  useEffect(() => {
    if (inView) {
      loadMoreRecs();
    }
  }, [inView]);

  return (
    <>
      <div
        className="grid min-[320px]:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-6"
        data-testid="recs"
      >
        {recommendations &&
          recommendations.map((item, index) => <Card key={'card-recomend-' + index} data={item} />)}
      </div>
      <div ref={ref} className="w-full">
        <Loading />
      </div>
    </>
  );
}
