import { test, expect, beforeEach, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import { LineChart } from './LineChart';

beforeEach(() => {
  HTMLCanvasElement.prototype.getContext = vi.fn() as typeof HTMLCanvasElement.prototype.getContext;
});

test('LineChart appears', () => {
  render(<LineChart id="" labels={[]} dataset={{ data: [] }} />);
  expect(screen.getByTestId('line-chart')).toBeDefined();
});
