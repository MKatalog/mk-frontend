'use client';

import Chart from 'chart.js/auto';
import { ChartDataset } from 'chart.js';
import { useEffect, useRef } from 'react';

export type LineChartProps = {
  id: string;
  labels: string[];
  dataset: ChartDataset;
  title?: string;
};

export function LineChart({ id, dataset, labels, title }: LineChartProps) {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const chartRef = useRef<Chart | null>(null);

  useEffect(() => {
    if (canvasRef.current) {
      const ctx = canvasRef.current.getContext('2d');

      if (ctx) {
        if (chartRef.current?.ctx) {
          chartRef.current.clear();
          chartRef.current.destroy();
        }
        chartRef.current = new Chart(ctx, {
          type: 'line',
          data: {
            labels,
            datasets: [dataset],
          },
          options: {
            plugins: {
              legend: {
                display: false,
              },
            },
            scales: {
              x: {
                ticks: {
                  autoSkip: false,
                  maxRotation: 0,
                  minRotation: 0,
                },
              },
            },
          },
        });
      }
    }

    return () => chartRef.current?.destroy();
  }, [canvasRef, dataset, labels]);

  return (
    <div className="w-full max-w-[600px]" data-testid="line-chart">
      {title && <h4 className="text-2xl leading-10">{title}</h4>}
      <canvas id={id} ref={canvasRef} className="max-h-[250px]" />
    </div>
  );
}
