import type { ProductCard } from '@/api/products';
import { DateTime } from 'luxon';

export const mockCards: ProductCard[] = [
  {
    id: '1',
    name: 'STM-32',
    productType: 'Микронотроллер STM',
    productLinksCount: 3,
    imagePath:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    prices: {
      from: 120,
      to: 390,
    },
    deliveryTimes: {
      from: DateTime.now().setLocale('ru').toISO(),
      to: DateTime.now().plus({ day: 2 }).toISO(),
    },
    quantites: {
      from: 19,
      to: 240,
    },
  },
  {
    id: '2',
    name: 'STM-32',
    productType: 'Микронотроллер STM',
    productLinksCount: 3,
    imagePath:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    prices: {
      from: 120,
      to: 390,
    },
    deliveryTimes: {
      from: DateTime.now().toISO(),
      to: DateTime.now().plus({ day: 2 }).toISO(),
    },
    quantites: {
      from: 19,
      to: 240,
    },
  },
  {
    id: '3',
    name: 'STM-32',
    productType: 'Микронотроллер STM',
    productLinksCount: 3,
    imagePath:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    prices: {
      from: 120,
      to: 390,
    },
    deliveryTimes: {
      from: DateTime.now().toISO(),
      to: DateTime.now().plus({ day: 2 }).toISO(),
    },
    quantites: {
      from: 19,
      to: 240,
    },
  },
  {
    id: '4',
    name: 'STM-32',
    productType: 'Микронотроллер STM',
    productLinksCount: 3,
    imagePath:
      'https://cdn.cnx-software.com/wp-content/uploads/2021/01/STM32H7XX-M7-Development-Board.jpg?lossy=1&ssl=1',
    prices: {
      from: 120,
      to: 390,
    },
    deliveryTimes: {
      from: DateTime.now().toISO(),
      to: DateTime.now().plus({ day: 2 }).toISO(),
    },
    quantites: {
      from: 19,
      to: 240,
    },
  },
];
