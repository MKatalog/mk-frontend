import type { Meta, StoryObj } from '@storybook/react';
import { Card } from './Card';
import { mockCards } from './mock';

const meta: Meta<typeof Card> = {
  component: Card,
  title: 'Molecules/Card',
};

export default meta;

type Story = StoryObj<typeof Card>;

export const Primary: Story = {
  render: () => <Card data={mockCards[0]} />,
};
