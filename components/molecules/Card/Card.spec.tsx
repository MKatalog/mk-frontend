import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Card } from './Card';
import { mockCards } from './mock';
import { TestRouter } from '@/components/templates/TestRouter';

test('Card appears', () => {
  render(
    <TestRouter router={{}}>
      <Card data={mockCards[0]} />
    </TestRouter>
  );
  expect(screen.getByTestId('card')).toBeDefined();
});
