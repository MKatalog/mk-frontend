'use client';

import { cn } from '@/lib/utils';
import { CardProps } from './types';
import { CompareIcon, HeartIcon, HeartIconVariant } from '@/components/atoms/Icons';
import Image from 'next/image';
import { DateTime } from 'luxon';
import { useRouter } from 'next/navigation';
import { useUnit } from 'effector-react';
import { $favoritesIds, addToFavorites, removeFromFavorites } from '@/api/favorites';
import { useEffect, useMemo, useState } from 'react';
import { $compareIds, addToCompare, removeFromCompare } from '@/api/compare';
import { BeetweenStatement } from '@/api/products';

export const defaultImg =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==';

export function Card({ data, className, ...props }: CardProps) {
  const [
    favorites,
    compares,
    addToFavoritesFn,
    removeFromFavoritesFn,
    addToCompareFn,
    removeFromCompareFn,
  ] = useUnit([
    $favoritesIds,
    $compareIds,
    addToFavorites,
    removeFromFavorites,
    addToCompare,
    removeFromCompare,
  ]);

  const [heartVariant, setHeartVariant] = useState<HeartIconVariant | undefined>();

  const { push } = useRouter();

  const onLikeClickHandler = () => {
    if (favorites.includes(data.id)) removeFromFavoritesFn(data.id);
    else addToFavoritesFn(data.id);
  };

  const onCompareClickHandler = () => {
    if (compares.includes(data.id)) removeFromCompareFn(data.id);
    else addToCompareFn(data.id);
  };

  const onCardClickHandler = () => {
    const params = new URLSearchParams();
    params.set('name', data.name);
    push(`product/${data.id}?${params.toString()}`);
  };

  const valueBySimilarity = (value: BeetweenStatement<string | number>) => {
    if (value.from === value.to) return value.from;

    return `${value.from} - ${value.to}`;
  };

  useEffect(() => {
    setHeartVariant(favorites.includes(data.id) ? 'fill' : 'default');
  }),
    [];

  return (
    <article
      {...props}
      className={cn(
        'transition-colors min-w-[300px] p-2 border-2 border-transparent rounded-md hover:border-gray-300',
        className
      )}
      data-testid="card"
    >
      <div className="flex justify-end items-center gap-1">
        <HeartIcon variant={heartVariant} onClick={onLikeClickHandler} />
        <CompareIcon className="w-[26px] h-[24px] cursor-pointer" onClick={onCompareClickHandler} />
      </div>
      <div className="w-[100%] cursor-pointer" onClick={onCardClickHandler}>
        <figure className="w-[100%] flex justify-center items-center">
          <Image
            src={data?.imagePath ?? defaultImg}
            alt={data.name}
            loading="lazy"
            quality={100}
            width={250}
            height={250}
          />
        </figure>
        <div className="w-[100%] mt-1">
          <h3 className="text-3xl text-wrap break-words">{data.name}</h3>
          <div className="grid grid-cols-2 [&>*:nth-child(odd)]:text-gray-500">
            <p>Цена:</p>
            <p>{`${valueBySimilarity(data.prices)} ₽`}</p>
            <p>Количество:</p>
            <p>{`${valueBySimilarity(data.quantites)} шт.`}</p>
            <p>Срок поставки:</p>
            <p>
              {valueBySimilarity({
                from: DateTime.fromISO(data.deliveryTimes.from)
                  .setLocale('ru')
                  .toFormat('dd.mm.yyyy'),
                to: DateTime.fromISO(data.deliveryTimes.to).setLocale('ru').toFormat('dd.mm.yyyy'),
              })}
            </p>
          </div>
          <p>
            Найдено в <span className="text-green-500">{data.productLinksCount}</span> магазинах
          </p>
        </div>
      </div>
    </article>
  );
}
