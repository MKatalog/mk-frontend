import type { ProductCard } from '@/api/products';
import { HTMLAttributes } from 'react';

export interface CardProps extends HTMLAttributes<HTMLDivElement> {
  data: ProductCard;
}
