import { Calendar } from '../../atoms/Calendar';
import { Popover, PopoverContent, PopoverTrigger, PopoverAnchor } from '../../atoms/Popover';
import { Input } from '../../atoms/Input';
import { DayPickerSingleProps } from 'react-day-picker';
import { format } from 'date-fns';
import { CalendarIcon } from '@/components/atoms/Icons';

export interface DatePickerProps {
  value?: Date;
  name?: string;
  onSelect?: (e: Date | undefined) => void;
  calendarProps?: Omit<DayPickerSingleProps, 'selected' | 'onSelect'>;
}

export function DatePicker({ calendarProps, onSelect, value, name }: DatePickerProps) {
  return (
    <Popover>
      <PopoverAnchor asChild>
        <div className="relative h-8 w-[100%]" data-testid="datepicker">
          <Input
            type="date"
            className="relative"
            name={name}
            value={format(value ?? new Date(), 'yyyy-MM-dd')}
            onChange={(e) => onSelect && onSelect(new Date(e.target.value))}
          />
          <PopoverTrigger>
            <CalendarIcon
              className="absolute right-2 top-0 bottom-0 m-auto"
              pathProps={{ className: 'dark:fill-white' }}
            />
          </PopoverTrigger>
        </div>
      </PopoverAnchor>
      <PopoverContent className="w-auto p-0">
        <Calendar selected={value} mode="single" onSelect={onSelect} {...calendarProps} />
      </PopoverContent>
    </Popover>
  );
}
