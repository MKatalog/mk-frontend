import type { Meta, StoryObj } from '@storybook/react';
import { DatePicker } from './DatePicker';
import { useState } from 'react';

const meta: Meta<typeof DatePicker> = {
  component: DatePicker,
  title: 'Molecules/DatePicker',
};

export default meta;

type Story = StoryObj<typeof DatePicker>;

const DatePickerFn = () => {
  const [date, setDate] = useState<Date | undefined>();

  return <DatePicker value={date} onSelect={setDate} />;
};

export const Primary: Story = {
  render: () => <DatePickerFn />,
};
