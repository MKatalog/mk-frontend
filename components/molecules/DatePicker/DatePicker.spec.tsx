import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { DatePicker } from './DatePicker';

test('DatePicker appears', () => {
  render(<DatePicker />);
  expect(screen.getByTestId('datepicker')).toBeDefined();
});
