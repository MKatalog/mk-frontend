import { test, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Carousel } from './Carousel';

test('Carousel appears', () => {
  render(<Carousel />);
  expect(screen.getByTestId('carousel')).toBeDefined();
});
