import type { Meta, StoryObj } from '@storybook/react';
import { SearchBar } from './SearchBar';
import { useState } from 'react';

const meta: Meta<typeof SearchBar> = {
  component: SearchBar,
  title: 'Molecules/SearchBar',
};

export default meta;

type Story = StoryObj<typeof SearchBar>;

export const Primary: Story = {
  render: () => <SearchBar />,
};
