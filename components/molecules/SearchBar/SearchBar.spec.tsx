import { test, expect, beforeEach, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import { SearchBar } from './SearchBar';
import { TestRouter } from '@/components/templates/TestRouter';

test('SearcgBar appears', () => {
  render(
    <TestRouter router={{}}>
      <SearchBar />
    </TestRouter>
  );
  expect(screen.getByTestId('search-bar')).toBeDefined();
});
