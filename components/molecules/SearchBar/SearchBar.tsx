'use client';

import { Button } from '@/components/atoms/Button';
import { CrossIcon, SearchIcon } from '@/components/atoms/Icons';
import { Input } from '@/components/atoms/Input';
import { cn } from '@/lib/utils';
import { usePathname, useSearchParams, useRouter } from 'next/navigation';
import { useState } from 'react';

export interface SearchBarProps {
  className?: string;
}

export function SearchBar({ className }: SearchBarProps) {
  const [focused, setFocused] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string | undefined>();

  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace, push } = useRouter();

  const onClearHandler = () => {
    setSearchValue('');
  };

  const onSearchHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter' && searchValue) {
      const params = new URLSearchParams(searchParams);
      params.set('text', searchValue ?? '');

      if (!pathname.includes('search')) push(`/search?${params.toString()}`);
      else replace(`${pathname}?${params.toString()}`);
    }

    return;
  };

  return (
    <div className={cn('relative items-center flex flex-row', className)} data-testid="search-bar">
      <SearchIcon
        className="absolute ml-1 mt-[3px]"
        pathProps={{
          className: 'stroke-gray-700 fill-gray-700 dark:fill-gray-300 dark:stroke-gray-300',
        }}
      />
      <Input
        className="px-10"
        placeholder="Введите текст"
        type="search"
        value={searchValue ?? ''}
        onChange={(e) => setSearchValue(e.target.value)}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
        onKeyUp={onSearchHandler}
      />
      <Button
        variant={'link'}
        size={'icon'}
        className={cn(
          'transition-opacity absolute right-1 mt-[1px] disabled:opacity-0',
          searchValue?.length ? '' : 'opacity-0 pointer-events-none',
          focused ? 'opacity-100' : 'opacity-0'
        )}
        disabled={!searchValue?.length}
        onClick={onClearHandler}
      >
        <CrossIcon
          className="group"
          pathProps={{
            className:
              'transition-colors stroke-gray-700 group-hover:stroke-gray-950 dark:stroke-gray-300 dark:group-hover:stroke-white',
          }}
        />
      </Button>
    </div>
  );
}
