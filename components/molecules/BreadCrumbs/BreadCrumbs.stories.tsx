import type { Meta, StoryObj } from '@storybook/react';
import { useState } from 'react';
import { BreadCrumbs } from './BreadCrumbs';

const meta: Meta<typeof BreadCrumbs> = {
  component: BreadCrumbs,
  title: 'Molecules/BreadCrumbs',
};

export default meta;

type Story = StoryObj<typeof BreadCrumbs>;

export const Primary: Story = {
  render: () => <BreadCrumbs />,
};
